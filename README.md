ansible-role-timezone
=====================

Set system default timezone.


Role Variables
--------------

- `timezone` : timezone (default: UTC)

Example Playbook
----------------

```yaml
- hosts: all
  roles:
    - role: timezone
      timezone: Asia/Tokyo
```

License
-------

Apache 2.0

Author Information
------------------

Based on Ryan Yates' [ansible-timezone](https://github.com/yatesr/ansible-timezone).
